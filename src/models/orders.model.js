// orders-model.js - A mongoose model
// 
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const orders = new Schema({
    product_id: { type: Schema.Types.ObjectId, required: true, ref: 'Product' },
    quantity: { type: Number, required: true },
    discount: { type: Number, required: true },
    customer_name: { type: String, required: true },
    bill_no: { type: String, required: true },
    price: { type: String, required: true },
    date: { type: String, required: true },
    payment_type: { type: String, required: true },
    sold_by: { type: Schema.Types.ObjectId, required: true, ref: 'User' },
  }, {
      timestamps: true
    });

  return mongooseClient.model('orders', orders);
};
