// products-model.js - A mongoose model
// 
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const products = new Schema({
    name: { type: String, required: true },
    brand_name: { type: String, required: true },
    description: { type: String, required: true },
    category: { type: String, required: true },
    supplier_id: { type: Schema.Types.ObjectId, required: true, ref: "Supplier" },
    unit_price: { type: Number, required: true },
    quantity: { type: Number, required: true },
    discount: { type: Number, required: false },
    weight: { type: String, required: true },
    nafdacno: { type: String, required: true },
    date: { type: String, required: true }
  }, {
      timestamps: true
    });

  return mongooseClient.model('products', products);
};
