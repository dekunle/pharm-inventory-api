// users-model.js - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const users = new mongooseClient.Schema({

    fullname: { type: String, required: true },
    dob: { type: String, required: true },
    address: { type: String, required: true },
    sex: { type: String, required: true },
    phone_no: { type: String, required: true },
    // role: { type: String, required: true },
    email: { type: String, unique: true, lowercase: true },
    password: { type: String },


  }, {
      timestamps: true
    });

  return mongooseClient.model('users', users);
};
