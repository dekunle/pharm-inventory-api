const { authenticate } = require('@feathersjs/authentication').hooks;
const { populate } = require('feathers-hooks-common');

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [
      populate({
        schema: {
          include: [{
            service: 'suppliers',
            nameAs: 'supplier',
            parentField: 'supplier_id',
            childField: '_id'
          }]
        }
      })
    ],
    get: [
      populate({
        schema: {
          include: [{
            service: 'suppliers',
            nameAs: 'supplier',
            parentField: 'supplier_id',
            childField: '_id'
          }]
        }
      })
    ],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
