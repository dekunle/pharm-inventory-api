const { authenticate } = require('@feathersjs/authentication').hooks;
const { populate } = require('feathers-hooks-common');

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [
      populate({
        schema: {
          include: [{
            service: 'products',
            nameAs: 'product',
            parentField: 'product_id',
            childField: '_id'
          },
          {
            service: 'users',
            nameAs: 'user',
            parentField: 'sold_by',
            childField: '_id'
          }
          ],
        }
      })
    ],
    get: [
      populate({
        schema: {
          include: [{
            service: 'products',
            nameAs: 'product',
            parentField: 'product_id',
            childField: '_id'
          },
          {
            service: 'users',
            nameAs: 'user',
            parentField: 'sold_by',
            childField: '_id'
          }
          ],
          // include: []
        }
      })
    ],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
