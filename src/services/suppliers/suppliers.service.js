// Initializes the `suppliers` service on path `/suppliers`
const createService = require('feathers-mongoose');
const createModel = require('../../models/suppliers.model');
const hooks = require('./suppliers.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/suppliers', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('suppliers');

  service.hooks(hooks);
};
