const authentication = require('@feathersjs/authentication');
const jwt = require('@feathersjs/authentication-jwt');
const local = require('@feathersjs/authentication-local');


module.exports = function (app) {
  const config = app.get('authentication');

  // Set up authentication with the secret
  app.configure(authentication(config));
  app.configure(jwt());
  app.configure(local());

  // The `authentication` service is used to create a JWT.
  // The before `create` hook registers strategies that can be used
  // to create a new valid JWT (e.g. local or oauth2)
  app.service('authentication').hooks({
    before: {
      create: [
        authentication.hooks.authenticate(config.strategies)
        // (hook) => {
        //   const resp =
        //    hook.params.payload = {
        //     userId: hook.params.users._id,
        //     email: hook.params.users.email,
        //   };
        //   console.log("+++++++++++++++");
        //   console.log(resp);
        //   console.log("+++++++++++++++");
        // }
      ],
      remove: [
        authentication.hooks.authenticate('jwt')
      ]
    },
    after: {
      create: (hook, next) => {
        const token = hook.result.accessToken;
        const base64Url = token.split('.')[1];
        const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
        const buff = new Buffer(base64, 'base64');
        const payloadinit = buff.toString('ascii');
        const user = JSON.parse(payloadinit);

        app.service('users').get(user.usersId).then(resp => {

          delete resp.password;

          hook.result.userData = resp;
          next();
        });

      }
    }
  });
};
